terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3"
    }
    random = {
      version = ">=3.0"
    }
  }
  backend "azurerm" {
    subscription_id = "9866517e-ffa1-427e-9726-299cc53cb81a"
    resource_group_name   = "rg-RMC701-sbx"
    storage_account_name  = "stormc701sbxtfs"
    container_name        = "tfstate"
    key = "akstest-02"
  }
}

provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
  skip_provider_registration = true
  
}