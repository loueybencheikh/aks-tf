resource "azurerm_user_assigned_identity" "mi-aks-cp" {
  name                = "mi-${var.prefix}-aks-cp"
  resource_group_name = var.resource_group_name
  location            = var.location
}


# AKS Cluster

module "aks" {
  source = "./modules/aks"
  

  resource_group_name = var.resource_group_name
  location            = var.location
  prefix              = "aks-${var.prefix}"
  mi_aks_cp_id        = azurerm_user_assigned_identity.mi-aks-cp.id
  k8s_version         = "1.29"

}

