# Creates cluster with default linux node pool

resource "azurerm_kubernetes_cluster" "akscluster" {
  name                                = var.prefix
  dns_prefix                          = var.prefix
  location                            = var.location
  resource_group_name                 = var.resource_group_name
  kubernetes_version                  = var.k8s_version
  private_cluster_enabled             = false
  azure_policy_enabled                = true
  sku_tier                            = "Free"
  

  default_node_pool {
    name            = "defaultpool"
    vm_size         = "Standard_B2s"
    os_disk_size_gb = 30
    max_count       = 3 
    type            = "VirtualMachineScaleSets"
    enable_auto_scaling = true
    min_count       = 2
     
    
  }

  network_profile {
    network_plugin     = var.network_plugin
    network_policy = var.network_policy
   
  }

  role_based_access_control_enabled = true

  azure_active_directory_role_based_access_control {
    managed = true
    
    //  admin_group_object_ids = talk to Ayo about this one, this arg could reduce code other places possibly 
    azure_rbac_enabled = true
  }

  identity {
    type         = "UserAssigned"
    identity_ids = [var.mi_aks_cp_id]
  }

  lifecycle {
    ignore_changes = [
      default_node_pool[0].node_count
    ]
  }
}

output "aks_id" {
  value = azurerm_kubernetes_cluster.akscluster.id
}

output "node_pool_rg" {
  value = azurerm_kubernetes_cluster.akscluster.node_resource_group
}

# Managed Identities created for Addons

output "kubelet_id" {
  value = azurerm_kubernetes_cluster.akscluster.kubelet_identity.0.object_id
}
