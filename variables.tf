variable "prefix" {
    default = "rmc701-SBX"
}

variable "state_sa_name" {
    default = "stormc701sbxtfs"
}

variable "container_name" {
    default = "tfstate"
}

variable "resource_group_name" {
    default = "rg-RMC701-sbx"
}

variable "subscription_id" {
    default = "9866517e-ffa1-427e-9726-299cc53cb81a"
  
}


variable "location" {
    default = "North Europe"
}

